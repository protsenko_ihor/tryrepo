﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace HW8_1
{
    public class LogOfRecords
    {

        public string NameUser;
        public string NameOfBook;
        public int DateHowLongBookWasTaken;
        public DateTime StartReading;
        public DateTime EndReading;
        
        //Список книг с информацией о клиенте, названием книги, периодом чтения, датой взятия и предположительной датой возрата книги
        public List<LogOfRecords> userReadBook = new List<LogOfRecords>();

        //Конструктор инициализации полей класа при создании экземпляра
        public LogOfRecords(string name, string book, int time, DateTime start, DateTime end)
        {
            NameUser = name;
            NameOfBook = book;
            DateHowLongBookWasTaken = time;
            StartReading = start;
            EndReading = end;
        }
        //Пустой конструктор для обращения к полям класса без предварительной инициализации
        public LogOfRecords()
        {

        }

        //Метод который выполняется выдачу книги, создания списка хранения выданых книг и списка с информацией о читателях
        public List<Book> GiveTheBookAddTimeForBook(List<Book> startlist, string name, int index, int time, List<LogOfRecords> readersBooks, List<Book> handsBooks)
        {
            startlist[index - 1].Time = startlist[index - 1].Time + time;
            DateTime start = DateTime.Now;
            DateTime end = start.AddDays(time);
            readersBooks.Add(new LogOfRecords(name, startlist[index - 1].NameOfBook, time, start, end));
            handsBooks.Add(startlist[index - 1]);
            startlist.RemoveAt(index - 1);
            return startlist;
        }

        //Метод для возращения книги в каталог от пользователя
        public List<Book> ReturnTheBookAddTimeForBook(List<Book> startlist, int index, List<LogOfRecords> readersBooks, List<Book> handsBooks)
        {
            handsBooks[index - 1].Time = readersBooks[index - 1].StartReading.Minute - DateTime.Now.Minute;
            startlist.Add(handsBooks[index - 1]);
            handsBooks.RemoveAt(index - 1);
            readersBooks.RemoveAt(index - 1);
            return startlist;

        }

    }
}
