﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW8_1
{
    public class AskInfo
    {
        //Метод для получения имени клиента
        public string AskUserName()
        {
            string nameClient;
            do
            {
                Console.WriteLine("Enter user name. At least 2 letters: ");
                nameClient = Console.ReadLine();

            }
            while (nameClient.Length < 2);
            return nameClient;
        }

        //Метод для получения значения названия книги
        public string AskUserNameOfBook()
        {
            string nameOfBook;
            do
            {
                Console.WriteLine("Enter name of book. At least 2 letters: ");
                nameOfBook = Console.ReadLine();
            }
            while (nameOfBook.Length < 2);
            return nameOfBook;
        }
        public string AskUserNameAutorOfBook()
        {
            string nameOfAutor;
            do
            {
                Console.WriteLine("Enter autor name. At least 2 letters: ");
                nameOfAutor = Console.ReadLine();
            }
            while (nameOfAutor.Length < 2);
            return nameOfAutor;
        }
        //Метод для получения значения выбранного пользователем действия 
        public int AskActionWithMainMenu()
        {
            int userChoise;
            bool isInputCorrect = false;
            do
            {
                Console.WriteLine("Select the action what you want to do with catalog!");
                isInputCorrect = int.TryParse(Console.ReadLine(), out userChoise);
                if (isInputCorrect == true && userChoise >= 0 && userChoise <= 11)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Your choise doesn't correct! Try again...");
                }
            } while (true);
            return userChoise;
        }
        public int AskUserNumberOfBook(List<Book> books)
        {
            int userChoise;
            bool isInputCorrect = false;
            do
            {
                Console.WriteLine("Select number of book!");
                isInputCorrect = int.TryParse(Console.ReadLine(), out userChoise);
                if (isInputCorrect == true && userChoise > 0 && userChoise <= books.Count)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Your choise doesn't correct! Try again...");
                }
            } while (true);
            return userChoise;
        }
        //Метод для получения значения значения периода, который пользователь хочет читать книгу
        public int AskUserHowLongTheyNeedTheBook()
        {
            int day;
            bool isInputCorrect = false;
            do
            {
                Console.WriteLine("How long the book will be with the reader: ");
                isInputCorrect = int.TryParse(Console.ReadLine(), out day);
                if (isInputCorrect == true && day > 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Your choise doesn't correct! Try again...");
                }
            } while (true);
            return day;
        }

    }

}
