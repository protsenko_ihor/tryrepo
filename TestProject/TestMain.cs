﻿
using HW8_1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    [TestFixture]
    public class TestMain
    {
        [Test]
        public void StartCatalogOfFiveBookTest( )
        {
            List<Book> booksAct = new List<Book>();
            booksAct.Add(new Book("Fight Club", "Chuck Palahniuk", 0));
            booksAct.Add(new Book("Snuff", "Viktor Pelevin", 0));
            booksAct.Add(new Book("A Scanner Darkly", "Philip Kindred Dick", 0));
            booksAct.Add(new Book("Heart of a Dog", "Mikhail Bulgakov", 0));
            booksAct.Add(new Book("The Idiot", "Fyodor Dostoevsky", 0));
                
            List<Book> booksResult = new List<Book>();
            List<Book> result = new СatalogBooks().StartCatalogOfFiveBook(booksResult);

            Assert.AreEqual(booksAct.Count, result.Count);

        }
                    [Test]
            public void AddBookTest()
            {
                List<Book> actual = new List<Book>();
                actual.Add(new Book("TestNameBook", "TestNameAuthor", 0));
                actual.Add(new Book("TestNameBook2", "TestNameAuthor2", 0));

                List<Book> resultS = new List<Book>();
                resultS.Add(new Book("TestNameBook", "TestNameAuthor", 0));
                List<Book> result = new СatalogBooks().AddBook(resultS, "TestNameBook2", "TestNameAuthor2");

                Assert.AreEqual(actual.Count, result.Count);
                Assert.AreEqual(actual[1].NameOfBook , result[1].NameOfBook);
                Assert.AreEqual(actual[1].NameOfAutor, result[1].NameOfAutor);
                Assert.AreEqual(actual[1].Time, result[1].Time);
        }


        }
    
}
