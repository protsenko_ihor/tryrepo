﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;


namespace HW8_1
{
    class MainClass
    {
        static void Main(string[] args)
        {
            // Команда Проценко Игорь, Воронков Евгений
            //        Задача 1.Библиотека.
            //        1 Часть
            //•    В библиотеке должно быть сначала 5 книг
            //•    Должна быть возможность добавить книгу в каталог
            //•    Исключить книгу из каталога
            //•    Вывести содержимое  каталога по алфавиту по автору и по названию книг
            //•    Вывести список книг определенного автора
            //•    Если таких книг нет написать об этом пользователю
            //•    *Найти книгу по слову или по нескольким словам в названии.
            //        2 Часть
            //•    Книгу можно взять почитать из библиотеки на определенное количество дней.
            //•    При этом должен быть журнал, где записано кто и на сколько дней взял книгу.
            //•    Общая статистика:  Вывести автора книги, название книги и сколько дней была у читателей в днях 
            //•    *Придумать ка реализовать подсчет дней когда книга не в библиотеке(таймер).
	
		    int choiseActionInCatalog;
            
            //Создание экземпляров классов
            AskInfo askUserItem = new AskInfo();
            СatalogBooks catalogItem = new СatalogBooks();
            LogOfRecords logbook = new LogOfRecords();

            catalogItem.StartCatalogOfFiveBook(catalogItem.books);


            do
            {
                Menu.ShowMenu();
                choiseActionInCatalog = askUserItem.AskActionWithMainMenu();
                switch (choiseActionInCatalog)
                {
               
                    case 1:
                        catalogItem.ShowAllBooks(catalogItem.books);
                        break;
                    case 2:
                        catalogItem.AddBook(catalogItem.books, askUserItem.AskUserNameOfBook(), askUserItem.AskUserNameAutorOfBook());
                        break;
                    case 3:
                        catalogItem.ShowAllBooks(catalogItem.books);
                        catalogItem.DeleteBook(catalogItem.books, askUserItem.AskUserNumberOfBook(catalogItem.books));
                        break;
                    case 4:
                        catalogItem.SortbyNameOfAutor(catalogItem.books);
                        break;
                    case 5:
                        catalogItem.SortbyNameOfBook(catalogItem.books);
                        break;
                    case 6:
                        catalogItem.FindbyNameOfAutor(catalogItem.books, askUserItem.AskUserNameAutorOfBook());
                        break;
                    case 7:
                        catalogItem.FindbyNameOfBook(catalogItem.books, askUserItem.AskUserNameOfBook());
                        break;
                    case 8:
                        catalogItem.ShowAllBooks(catalogItem.books);
                        logbook.GiveTheBookAddTimeForBook(catalogItem.books, askUserItem.AskUserName(), askUserItem.AskUserNumberOfBook(catalogItem.books), askUserItem.AskUserHowLongTheyNeedTheBook(), logbook.userReadBook, catalogItem.handsBooks);
                        break;
                    case 9:
                        catalogItem.ShowAllBooksHandsOfUsers(logbook.userReadBook);
                        if(logbook.userReadBook.Count > 0)
                        {
                            logbook.ReturnTheBookAddTimeForBook(catalogItem.books, askUserItem.AskUserNumberOfBook(catalogItem.books), logbook.userReadBook, catalogItem.handsBooks);
                        }
                        break;
                    case 10:
                        catalogItem.ShowAllBooksHandsOfUsers(logbook.userReadBook);
                        break;
                    case 11:
                        catalogItem.ShowStatisticsForAllBooks(catalogItem.books);
                        break;
                }
            }
            while (choiseActionInCatalog != 0);
            Console.WriteLine("Bye-Bye!");
        }
    }
    
}
