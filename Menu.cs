using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW8_1
{
    class Menu
    {

	//Вывод меню выбора действия с каталогом
        public static void ShowMenu()
        {
            Console.WriteLine();
            Console.WriteLine("List of available items: ");
            Console.WriteLine("1 - Show books in catalog");
            Console.WriteLine("2 - Add book to catalog");
            Console.WriteLine("3 - Remove book from catalog");
	        Console.WriteLine("4 - Display the contents of the directory alphabetically by the author of the book");
            Console.WriteLine("5 - Display directory contents by book title");
            Console.WriteLine("6 - Find a book by a word or by several words by a specific author to the console");
            Console.WriteLine("7 - Find a book by a word or by several words by the name of book");
            Console.WriteLine("8 - Give the book to the client");
            Console.WriteLine("9 - Return the book to the catalog");
            Console.WriteLine("10 - Statistics by people who took the book");
            Console.WriteLine("11 - Statistics by books and people who took it!");
            Console.WriteLine("0 - Exit");
            Console.WriteLine();

        }
            
    }

        

}
