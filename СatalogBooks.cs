﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW8_1
{
    public class СatalogBooks
    {
        //Список экземпляров класса Book, который содержит информацию всех книг
        public List<Book> books = new List<Book>();
        public List<Book> handsBooks = new List<Book>();

        //Метод наполнения каталога первыми 5ю книгами
        public List<Book> StartCatalogOfFiveBook(List<Book> books)
        {
            books.Add(new Book("Fight Club", "Chuck Palahniuk", 0));
            books.Add(new Book("Snuff", "Viktor Pelevin", 0));
            books.Add(new Book("A Scanner Darkly", "Philip Kindred Dick", 0));
            books.Add(new Book("Heart of a Dog", "Mikhail Bulgakov", 0));
            books.Add(new Book("The Idiot", "Fyodor Dostoevsky", 0));
            return books;
        }

        //Метод добавления книги в каталог
        public List<Book> AddBook(List<Book> books, string bookName, string autorName)
        {
            //Console.Clear();
            int time = 0;
            books.Add(new Book(bookName, autorName, time));
            //Console.WriteLine($"Name of book: {bookName}. Autor: {autorName}. Book was added!");
            return books;
        }

        //Метод удаления книги из каталога
        public List<Book> DeleteBook(List<Book> books, int idBook)
        {
            Console.Clear();
            Console.WriteLine($"Name of book: {books[idBook - 1].NameOfBook}. Autor: {books[idBook - 1].NameOfAutor}. Book was Deleted!");
            books.RemoveAt(idBook - 1);

            return books;
        }
        //Метод вывода всех книг из каталога с основной информацией - название книги и ее автор.
        public void ShowAllBooks(List<Book> books)
        {
            Console.Clear();
            foreach (Book element in books)
            {
                Console.WriteLine($"ID: {books.IndexOf(element) + 1} Name of book: {element.NameOfBook}. Autor: {element.NameOfAutor}");
            }
            Console.WriteLine();
        }
        //Метод для вывода списка книг с информацией о клиенте, который взял ее почитать
        public void ShowAllBooksHandsOfUsers(List<LogOfRecords> books)
        {

            Console.Clear();
            if (books.Count > 0)
            {
                foreach (LogOfRecords element in books)
                {
                    //Console.WriteLine($"ID: {books.IndexOf(element) + 1} Name of user: {element.NameUser}. Name of book: {element.NameOfBook}. How Long the book on hand: {element.DateHowLongBookWasTaken} day(s)");
                    Console.WriteLine($"ID:{books.IndexOf(element) + 1}");
                    Console.WriteLine($"Name of user: {element.NameUser}. Name of book: {element.NameOfBook}.");
                    Console.WriteLine($"Date when the book was taken: {element.StartReading}.");
                    Console.WriteLine($"Date when the book should be returned: {element.EndReading}");
                    Console.WriteLine($"The number of days that the book has been with the reader: {element.DateHowLongBookWasTaken} day(s)");
                    Console.WriteLine("-------------");
                }
            }
            else
            {
                Console.WriteLine($"No books in the hands of readers!");
            }
            Console.WriteLine();

        }

        //Метод для вывода списка книг с полной статистикой
        public void ShowStatisticsForAllBooks(List<Book> books)
        {
            Console.Clear();
            foreach (Book someBook in books)
            {
                Console.WriteLine($"ID: {books.IndexOf(someBook) + 1} Name of book: {someBook.NameOfBook}. Autor: {someBook.NameOfAutor}. The number of days the readers had: {someBook.Time} day(s)");
            }
            Console.WriteLine();
        }

        //Метод сортировки по названию книги
        public void SortbyNameOfBook(List<Book> books)
        {
            Console.Clear();
            Console.WriteLine("The contents of the catalog alphabetically by the name of the book");
            books = books.OrderBy(item => item.NameOfBook.Split(' ').ElementAtOrDefault(0)).ToList();
            foreach (Book element in books)
            {
                Console.WriteLine($"ID: {books.IndexOf(element) + 1} Name of book: {element.NameOfBook}. Autor: {element.NameOfAutor}");
            }
            Console.WriteLine();
        }
        //Метод сортировки по автору книги
        public void SortbyNameOfAutor(List<Book> books)
        {
            Console.Clear();
            Console.WriteLine("The contents of the catalog alphabetically by the author of the book");
            books = books.OrderBy(item => item.NameOfAutor.Split(' ').ElementAtOrDefault(0)).ToList();
            foreach (Book element in books)
            {
                Console.WriteLine($"ID: {books.IndexOf(element) + 1} Name of book: {element.NameOfBook}. Autor: {element.NameOfAutor}");
            }
            Console.WriteLine();
        }
        //Метод вывода списка книг определенного автора
        public void FindbyNameOfAutor(List<Book> books, string nameAutor)
        {
            Console.Clear();
            Console.WriteLine("Sort catalog by a specific author to the console");
            int count = 0;
            foreach (Book element in books)
            {
                if (element.NameOfAutor.Contains(nameAutor))
                {
                    count++;
                    Console.WriteLine($"ID: {books.IndexOf(element) + 1} Name of book: {element.NameOfBook}. Autor: {element.NameOfAutor}");
                }
                else
                    continue;
            }
            if (count == 0)
            {
                Console.WriteLine("No match! Try to find again!");
            }
            Console.WriteLine();
        }
        //Метод поиска книги по слову или по нескольким словам в названии
        public void FindbyNameOfBook(List<Book> books, string nameBook)
        {
            Console.Clear();
            Console.WriteLine("Sort catalog by the name of book to the console");
            int count = 0;
            foreach (Book element in books)
            {
                if (element.NameOfBook.Contains(nameBook))
                {
                    count++;
                    Console.WriteLine($"ID: {books.IndexOf(element) + 1} Name of book: {element.NameOfBook}. Autor: {element.NameOfAutor}");
                }
                else
                    continue;
            }
            if (count == 0)
            {
                Console.WriteLine("No match! Try to find again!");
            }
            Console.WriteLine();
        }
    }
}
